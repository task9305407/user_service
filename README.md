# User Serviceni Ishlatish
## Birinchi Qadam
**Birinchi o'rinda databaseni to'g'irlab olish lozim ya'ni user_service uchun database yaratish kerak postgreSql da.**

```
CREATE DATABASE dbname;
```
## Ikkinchi Qadam
**Databaseda table yaratish uchun migratsiyani ishlatib yuboramiz quyidagicha:**
```
make migrate_up
```

## Ishga tushurish quyidagicha bo'ladi:
```
make run
```
