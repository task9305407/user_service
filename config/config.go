package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	GrpcPort string
	GrpsHost string
	LogLevel string
	PostgresConfig
}

type PostgresConfig struct {
	PostgresUser     string
	PostgresPassword string
	PostgresDatabase string
	PostgresHost     string
	PostgresPort     int
}

func Load() Config {
	c := Config{}
	c.GrpcPort = cast.ToString(getOrReturnDefault("GRPC_PORT", "5000"))
	c.GrpsHost = cast.ToString(getOrReturnDefault("GRPC_HOST", "localhost"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.PostgresConfig.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	c.PostgresConfig.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "password"))
	c.PostgresConfig.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "dbname"))
	c.PostgresConfig.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresConfig.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
