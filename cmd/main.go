package main

import (
	"net"

	"gitlab.com/task/user_service/genproto/user"
	"gitlab.com/task/user_service/pkg/db"
	"gitlab.com/task/user_service/pkg/logger"

	"gitlab.com/task/user_service/internal/controller/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/task/user_service/config"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "golang")
	defer logger.Cleanup(log)
	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresConfig.PostgresHost),
		logger.String("port", cfg.PostgresConfig.PostgresHost),
		logger.String("datab", cfg.PostgresConfig.PostgresDatabase),
	)

	connDb, err := db.ConnectToDB(cfg)

	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}

	userService := service.NewUserService(connDb, log, &cfg)

	lis, err := net.Listen("tcp", ":"+cfg.GrpcPort)

	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)

	user.RegisterUserServiceServer(s, userService)

	log.Info("main: server running",
		logger.String("port", cfg.GrpcPort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
