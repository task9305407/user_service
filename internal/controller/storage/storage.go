package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/task/user_service/internal/controller/storage/postgres"
	"gitlab.com/task/user_service/internal/controller/storage/repo"
)

type StorageI interface {
	User() repo.UserStorageI
}

type storagePg struct {
	userRepo repo.UserStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		userRepo: postgres.NewUser(db),
	}
}

func (s *storagePg) User() repo.UserStorageI {
	return s.userRepo
}
