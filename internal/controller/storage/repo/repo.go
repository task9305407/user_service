package repo

import "gitlab.com/task/user_service/genproto/user"

type UserStorageI interface {
	CreateUser(*user.UserRequest) (*user.UserResponse, error)
	FindById(*user.UserId) (*user.UserResponse, error)
	FindAll(*user.AllUsersParamsRequest) (*user.AllUsersParamsResponse, error)
}
