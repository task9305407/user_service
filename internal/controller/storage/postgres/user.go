package postgres

import (
	"database/sql"
	"log"

	us "gitlab.com/task/user_service/genproto/user"
)

func (u *userRepo) CreateUser(user *us.UserRequest) (*us.UserResponse, error) {
	var res us.UserResponse
	query := `
	INSERT INTO 
		users (name, age, phone) 
	VALUES
		($1, $2, $3) 
	RETURNING 
		id, name, age, phone
	`
	err := u.Db.QueryRow(query, user.Name, user.Age, user.Phone).
		Scan(&res.Id, &res.Name, &res.Age, &res.Phone)
	if err != nil {
		log.Printf("Error while inserting user info")
		return &us.UserResponse{}, err
	}
	return &res, nil
}

func (u *userRepo) FindById(user *us.UserId) (*us.UserResponse, error) {
	var res us.UserResponse
	query := `
	SELECT 
		id, name, age, phone
	FROM 
		users 
	WHERE 
		id=$1
	`
	err := u.Db.QueryRow(query, user.Id).
		Scan(&res.Id, &res.Name, &res.Age, &res.Phone)
	if err == sql.ErrNoRows {
		return &us.UserResponse{}, nil
	}
	return &res, nil
}

func (u *userRepo) FindAll(user *us.AllUsersParamsRequest) (*us.AllUsersParamsResponse, error) {
	var res us.AllUsersParamsResponse
	query := `
	SELECT 
		id, name, age, phone 
	FROM 
		users
	WHERE 
		name ILIKE $1 LIMIT $2 OFFSET $3
	`
	rows, err := u.Db.Query(query, "%"+user.Keyword+"%", user.Limit, (user.Page-1)*user.Limit)
	if err != nil {
		log.Println("error while getting all users")
		return &us.AllUsersParamsResponse{}, err
	}
	for rows.Next() {
		temp := us.UserResponse{}
		err = rows.Scan(&temp.Id, &temp.Name, &temp.Age, &temp.Phone)
		if err != nil {
			return &us.AllUsersParamsResponse{}, err
		}
		res.Users = append(res.Users, &temp)
	}
	return &res, nil
}
