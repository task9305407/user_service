package postgres

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/task/user_service/internal/controller/storage/repo"
)

type userRepo struct {
	Db *sqlx.DB
}

func NewUser(db *sqlx.DB) repo.UserStorageI {
	return &userRepo{
		Db: db,
	}
}
