package service

import (
	"context"

	us "gitlab.com/task/user_service/genproto/user"
	"gitlab.com/task/user_service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *UserService) CreateUser(ctx context.Context, user *us.UserRequest) (*us.UserResponse, error) {
	res, err := s.storage.User().CreateUser(user)
	if err != nil {
		s.logger.Error("error while inserting user", logger.Any("Error insert user", err))
		return &us.UserResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return res, nil
}

func (s *UserService) FindById(ctx context.Context, req *us.UserId) (*us.UserResponse, error) {
	res, err := s.storage.User().FindById(req)
	if err != nil {
		s.logger.Error("error while getting user by id", logger.Any("Error get user", err))
		return &us.UserResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return res, nil
}

func (s *UserService) FindAll(ctx context.Context, req *us.AllUsersParamsRequest) (*us.AllUsersParamsResponse, error) {
	res, err := s.storage.User().FindAll(req)
	if err != nil {
		s.logger.Error("error while getting all user", logger.Any("Error getting all user", err))
		return &us.AllUsersParamsResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}

	return res, nil
}
