package service

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/task/user_service/config"
	"gitlab.com/task/user_service/internal/controller/storage"
	"gitlab.com/task/user_service/pkg/logger"
)

type UserService struct {
	storage storage.StorageI
	logger  logger.Logger
	cfg     *config.Config
}

func NewUserService(db *sqlx.DB, log logger.Logger, cfg *config.Config) *UserService {
	return &UserService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		cfg:     cfg,
	}

}
