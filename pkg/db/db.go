package db

import (
	"fmt"

	"gitlab.com/task/user_service/config"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func ConnectToDBForSuite(cfg config.Config) (*sqlx.DB, func()) {
	psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresConfig.PostgresHost,
		cfg.PostgresConfig.PostgresPort,
		cfg.PostgresConfig.PostgresUser,
		cfg.PostgresConfig.PostgresPassword,
		cfg.PostgresConfig.PostgresDatabase,
	)

	connDb, err := sqlx.Connect("postgres", psqlString)
	if err != nil {
		return nil, func() {}
	}
	cleanUpfunc := func() {
		connDb.Close()
	}

	return connDb, cleanUpfunc
}

func ConnectToDB(cfg config.Config) (*sqlx.DB, error) {
	psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresConfig.PostgresHost,
		cfg.PostgresConfig.PostgresPort,
		cfg.PostgresConfig.PostgresUser,
		cfg.PostgresConfig.PostgresPassword,
		cfg.PostgresConfig.PostgresDatabase,
	)

	connDb, err := sqlx.Connect("postgres", psqlString)
	if err != nil {
		return nil, err
	}

	return connDb, nil
}
